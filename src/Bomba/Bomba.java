/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Bomba;
public class Bomba {
    private int idBomba;
    private int capacidad;
    private int contadorLitros;
    private Gasolina gasolina;

   public Bomba(){
       this.idBomba = 0;
       this.capacidad = 0;
       this.contadorLitros = 0;
       this.gasolina = new Gasolina();
   }
    public Bomba(int idBomba, int capacidad, int contadorLitros, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.capacidad = capacidad;
        this.contadorLitros = contadorLitros;
        this.gasolina = gasolina;
    }

    public int getIdBomba() {
        return idBomba;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getContadorLitros() {
        return contadorLitros;
    }

    public void setContadorLitros(int contadorLitros) {
        this.contadorLitros = contadorLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
 //metodos de comportamineto
    public float obtenerInventario(){
        return this.capacidad - this.contadorLitros;
    }
    
    public float venderGasolina(int cantidad){
        float totalVenta = 0.0f;
        if(cantidad <= this.obtenerInventario()){
            totalVenta = cantidad * this.gasolina.getPrecio();
            this.contadorLitros = this.contadorLitros + cantidad;
        }
        return totalVenta;
    }
    public float totalVenta(){
        return this.contadorLitros * this.gasolina.getPrecio();
    }
} 
