/*
Descripcion: se desa saber el area y perimetro sobre un terreno para calcular la cotizacion sobre caunto semilla necesita
comprar para sembrar 
Nombre: Emilio Josue Garcia Elizarraras 
Fecha: 13-05-2024
 */
package misclases02;
import java.util.Scanner;
public class Terreno {
    //Declaracion de atributos
    private int numeroL;
    private float metroA;
    private float metroL;
    
    //constructor vacio
    public Terreno(){
        this.numeroL = 0;
        this.metroA = 0.0f;
        this.metroL = 0.0f;
       
    }
    //constructor de argumentos

    public Terreno(int numeroL, float metroA, float metroL) {
        this.numeroL = numeroL;
        this.metroA = metroA;
        this.metroL = metroL;
    }
    //constructor de copia
    public Terreno (Terreno t){
        this.numeroL = t.numeroL;
        this.metroA = t.metroA;
        this.metroL = t.numeroL;
    }

    public int getNumeroL() {
        return numeroL;
    }

    public void setNumeroL(int numeroL) {
        this.numeroL = numeroL;
    }

    public float getMetroA() {
        return metroA;
    }

    public void setMetroA(float metroA) {
        this.metroA = metroA;
    }

    public float getMetroL() {
        return metroL;
    }

    public void setMetroL(float metroL) {
        this.metroL = metroL;
    }
    //metodos de comportamieinto
    public float calcularperimetro(){
        return this.metroA * 2 + this.metroL ;
    }
    public float calcularArea(){
        return this.metroA * this.metroL;
    }
    
    
  
    
}
