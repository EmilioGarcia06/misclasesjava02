/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

public class EmpleadoEventual extends Empleado{
    private float pagoHoras;
    private float hrsTrabajadas;

    public EmpleadoEventual(float pagoHoras, float hrsTrabajadas, int numEmpleado, String nombre, String Puesto, String depto) {
        super(numEmpleado, nombre, Puesto, depto);
        this.pagoHoras = pagoHoras;
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    public EmpleadoEventual(){
        super ();
        this.pagoHoras = 0.0f ;
        this.hrsTrabajadas = 0.0f;
    }

    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public float getHrsTrabajadas() {
        return hrsTrabajadas;
    }

    public void setHrsTrabajadas(float hrsTrabajadas) {
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    
    @Override
    public float calcularPago() {
    float pago = 0.0f;
    pago = this.hrsTrabajadas * this.pagoHoras;
    return pago;
    
      
    }
}
