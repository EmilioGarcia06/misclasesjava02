/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;


public abstract class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String Puesto;
    protected String depto;
   
    //constructores

    public Empleado(int numEmpleado, String nombre, String Puesto, String depto) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.Puesto = Puesto;
        this.depto = depto;
    }

    public Empleado() {
         this.numEmpleado = 0;
        this.nombre = "";
        this.Puesto = "";
        this.depto = "";
    }
    //set y get

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return Puesto;
    }

    public void setPuesto(String Puesto) {
        this.Puesto = Puesto;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }
    
    public abstract float calcularPago();
    
}
