/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;
public class EmpleadoBase extends Empleado implements impuestos {
    private float pagoDiario;
    private float diasTrabajados;
    
    //constructorees

    public EmpleadoBase(float pagoDiario, float diasTrabajados, int numEmpleado, String nombre, String Puesto, String depto) {
        super(numEmpleado, nombre, Puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
    
    public EmpleadoBase(){
        super();
        this.diasTrabajados = 0.0f;
        this.pagoDiario = 0.0f;
        
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
   
    @Override
    public float calcularPago() {
    return this.diasTrabajados * this.pagoDiario;
    }

    @Override
    public float calcularImpuestos() {
        float pago = 0;
        if(this.calcularPago()>= 15000) pago = this.calcularPago() * 0.15f;
        return pago;
    }
    
}
